package com.company.project.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.company.project.common.utils.DataResult;
import com.company.project.entity.BdShareEntity;
import com.company.project.service.BdShareService;
import com.company.project.service.HttpSessionService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;


/**
 * 连接分享
 *
 * @author wenbin
 * @email *****@mail.com
 * @date 2020-09-07 14:59:32
 */
@RestController
@RequestMapping("/")
public class BdShareController {
    @Autowired
    private BdShareService bdShareService;

    @Lazy
    @Resource
    private HttpSessionService httpSessionService;


    @ApiOperation(value = "新增")
    @PostMapping("bdShare/add")
    @RequiresPermissions("bdShare:add")
    public DataResult add(@RequestBody BdShareEntity bdShare) {
        bdShare.setCreateTime(new Date());
        bdShare.setCreateId(httpSessionService.getCurrentUserId());
        bdShareService.save(bdShare);
        return DataResult.success();
    }

    @ApiOperation(value = "删除")
    @DeleteMapping("bdShare/delete")
    @RequiresPermissions("bdShare:delete")
    public DataResult delete(@RequestBody @ApiParam(value = "id集合") List<String> ids) {

        bdShareService.removeByIds(ids);
        return DataResult.success();
    }

    @ApiOperation(value = "更新")
    @PutMapping("bdShare/update")
    @RequiresPermissions("bdShare:update")
    public DataResult update(@RequestBody BdShareEntity bdShare) {
        bdShare.setUpdateTime(new Date());
        bdShare.setUpdateId(httpSessionService.getCurrentUserId());
        bdShareService.updateById(bdShare);
        return DataResult.success();
    }

    @ApiOperation(value = "查询分页数据")
    @PostMapping("bdShare/listByPage")
    @RequiresPermissions("bdShare:list")
    public DataResult findListByPage(@RequestBody BdShareEntity bdShare) {
        Page page = new Page(bdShare.getPage(), bdShare.getLimit());
        LambdaQueryWrapper<BdShareEntity> queryWrapper = Wrappers.lambdaQuery();
        //查询条件示例
        if (StrUtil.isNotBlank(bdShare.getTitle())){
            queryWrapper.like(BdShareEntity::getTitle,bdShare.getTitle());
        }
        IPage<BdShareEntity> iPage = bdShareService.page(page, queryWrapper);
        return DataResult.success(iPage);
    }

}
