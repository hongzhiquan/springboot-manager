package com.company.project.mapper;

import com.company.project.entity.BdShareEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 连接分享
 * 
 * @author wenbin
 * @email *****@mail.com
 * @date 2020-09-07 14:59:32
 */
public interface BdShareMapper extends BaseMapper<BdShareEntity> {
	
}
