package com.company.project.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.company.project.vo.req.PageReqVO;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * 连接分享
 *
 * @author wenbin
 * @email *****@mail.com
 * @date 2020-09-07 14:59:32
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("bd_share")
public class BdShareEntity extends PageReqVO implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 用户id
	 */
	@TableId("id")
	private String id;

	/**
	 * 百度云连接
	 */
	@TableField("url")
	private String url;

	/**
	 * 分享者id
	 */
	@TableField("user_id")
	private String userId;

	/**
	 * 状态，0未发布，1 已发布，2 被举报
	 */
	@TableField("status")
	private Integer status;

	/**
	 * 分享连接标题
	 */
	@TableField("title")
	private String title;

	/**
	 * 是否删除(1未删除；0已删除)
	 */
	@TableField("deleted")
	private Integer deleted;

	/**
	 * 创建人
	 */
	@TableField("create_id")
	private String createId;

	/**
	 * 更新人
	 */
	@TableField("update_id")
	private String updateId;

	/**
	 * 创建来源(1.web 2.android 3.ios )
	 */
	@TableField("create_where")
	private Integer createWhere;

	/**
	 * 创建时间
	 */
	@TableField("create_time")
	private Date createTime;

	/**
	 * 
	 */
	@TableField("update_time")
	private Date updateTime;


}
