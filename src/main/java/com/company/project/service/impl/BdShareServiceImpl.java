package com.company.project.service.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.company.project.mapper.BdShareMapper;
import com.company.project.entity.BdShareEntity;
import com.company.project.service.BdShareService;


@Service("bdShareService")
public class BdShareServiceImpl extends ServiceImpl<BdShareMapper, BdShareEntity> implements BdShareService {


}