package com.company.project.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.company.project.entity.BdShareEntity;

/**
 * 连接分享
 *
 * @author wenbin
 * @email *****@mail.com
 * @date 2020-09-07 14:59:32
 */
public interface BdShareService extends IService<BdShareEntity> {

}

